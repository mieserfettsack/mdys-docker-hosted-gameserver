#!/bin/bash

timedatectl set-timezone Europe/Berlin
apt-get -yqq update
apt-get -yqq install rsync openssh-server software-properties-common curl ca-certificates gnupg python3-pip locales locales-all  < /dev/null > /dev/null
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get -yqq update
apt-get -yqq install docker-ce docker-ce-cli btrfs-tools < /dev/null > /dev/null
pip3 -q install setuptools
pip3 -q install awscli awscli-plugin-endpoint
sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
fallocate -l 4G /swapfile
dd if=/dev/zero of=/swapfile bs=1024 count=4194304
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
echo "UUID=5d182973-669b-4e91-b6ea-385c6de148b1 /var/lib/docker btrfs defaults 0 0" >> /etc/fstab