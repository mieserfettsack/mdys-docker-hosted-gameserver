//Phrases for Ultimate Mapchooser by Steell
//Portugese translation by away000

"Phrases"
{
    "Slot Block Message 1"
    {
        "pt"    "Considere que mapa voce gostaria de jogar"
    }

    "Slot Block Message 2"
    {
        "pt"    "e n�o aperte os bot�es atoa."
    }

    "Map Vote Menu Title"
    {
        "pt"    "Vote para o pr�ximo mapa!"
    }

    "Group Vote Menu Title"
    {
        "pt"    "Vote para o pr�ximo grupo!"
    }

    "Extend"
    {
        "pt"    "Exntender o mapa"
    }

    "Don't Change"
    {
        "pt"    "N�o mudar"
    }


    "End of Map Vote Over"
    {
        "pt"    "Vota��o para o mapa encerrada."
    }

    "Map Extended"
    {
        "pt"    "O mapa atual ser� extendido."
    }

    "End of Map Vote Map Won"
    {
        "pt"    "O pr�ximo mapa ser� {1}."
    }

    "Vote Win Percentage"
    {
        //There are supposed to be two percent signs here.
        //If there is only one, no percent sign will be displayed.
        "pt"    "Recebeu {1}%% de {2} votos"
    }

    "Map Unchanged"
    {
        "pt"    "O mapa n�o ser� mudado."
    }

    "RTV Map Won"
    {
        "pt"    "Mudando mapa para {1}."
    }

    "Map Change in 5"
    {
        "pt"    "Mudando mapa em 5 segundos."
    }

    "Map Change at Round End"
    {
        "pt"    "O mapa ir� mudar no fim desta rodada."
    }

    "End of Map Vote Group Won"
    {
        "pt"    "O pr�ximo mapa ser� '{1},' escolhido do grupo '{2}.'"
    }

    "RTV Group Won"
    {
        "pt"    "Mudando mapa para '{1},' escolhido do grupo '{2}.'"
    }

    "Runoff Msg"
    {
        "pt"    "A vota��o falhou! N�o foi alcancado o numero de votos nescess�rios."
    }

    "Another Vote"
    {
        "pt"    "Outra vota��o ir� iniciar em {1} segundos."
    }

    "Player Disconnect RTV"
    {
        "pt"    "Jogador desconectado usou Rock the Vote."
    }

    "Not Enough Players"
    {
        "pt"    "N�o tem jogadores o suficiente no servidor para o mapa atual. ({1} nescess�rios)"
    }

    "Too Many Players"
    {
        "pt"    "Excesso de jogadores no servidor para o mapa  atual. ({1} m�ximo)"
    }

    "No RTV Player Count"
    {
        "pt"    "N�o � possivel o Rock the Vote antes de ter mais do que {1} jogadores conectados no servidor."
    }

    "No RTV Nextmap"
    {
        "pt"    "N�o � possivel o Rock the Vote antes do pr�ximo mapa."
    }

    "No RTV Time"
    {
        "pt"    "N�o � possivel o Rock the Vote por mais {1} segundos."
    }

    "More Required"
    {
        "pt"    "{1} mais s�o nescess�rios"
    }

    "RTV Entered"
    {
        "pt"    "{1} quer o Rock the Vote."
    }

    "RTV Info Msg"
    {
        "pt"    "Digite 'rtv' para iniciar a vota��o, ou digite 'nominate' para nominar um mapa."
    }

    "RTV Start"
    {
        "pt"    "Rocking the Vote!"
    }

    "Vote In Progress"
    {
        "pt"    "Vota��o j� em progresso."
    }

    "RTV Already Entered"
    {
        "pt"    "Voce j� usou o Rock the Vote."
    }

    "No Nominate Nextmap"
    {
        "pt"    "N�o � poss�vel nominar antes do pr�ximo mapa."
    }

    "Pending Vote"
    {
        "pt"    "Vota��o pendente"
    }

    "Next Map"
    {
        "pt"    "O pr�ximo mapa ser�: {1}"
    }

    "Player Nomination"
    {
        "pt"    "{1} nominou o mapa {2}."
    }

    "Nomination Menu Title"
    {
        "pt"    "Nominou um mapa."
    }

    "Yes/No Menu Title"
    {
        "pt"    "Mudando o mapa para {1}?"
    }

    "Yes"
    {
        "pt"    "Sim"
    }

    "No"
    {
        "pt"    "N�o"
    }

    "Starting Map Vote"
    {
        "pt"    "Iniciando vota��o para o mapa. O mapa ir� mudar apos a vota��o."
    }

    "Default Warning"
    {
        //{TIME} is where the time in seconds is going to be placed.
        //Please keep {TIME} in this string, it is not an error.
        "pt"    "{TIME} segundos antes da vota��o."
    }
}
