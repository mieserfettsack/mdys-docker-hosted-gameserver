"Phrases"
{
	"Start Hiding"
	{
		"fr"		"{unique}La partie de cache-cache ... COMMENCE !"
		"en"		"{unique}Round-End Prop Hunt ACTIVE !"
	}

	"Hiding in"
	{
		"fr"		"Vous etes deguise en"
		"en"		"You are disguised as a"
	}

	"Run hiding"
	{
		"fr"		"{unique}Courrez-vous cacher !"
		"en"		"{unique}Run you hide !"
	}
	
	"Third Person"
	{
		"fr"		"Tapez {community}!third/!thirdperson{normal} dans le chat pour avoir une vue à la troisieme personne !"
		"en"		"Write {community}!third/!thirdperson{normal} on the chat to have the third view!"
	}

}
