/****************************************************************************************************
	[ANY] EDICT OVERFLOW FIX v2.3
*****************************************************************************************************/

/****************************************************************************************************
	CHANGELOG
*****************************************************************************************************
* 
* 2.0 	     - 
* 				Started Development again.
* 2.1 (Beta) - 
* 				Use Percentages.
* 2.2 (Beta) - 
* 				Remove func_ check as its not the only thing that will cause crashes.
* 				Ignore Edicts when there is more than 256 available.
* 2.3 - 
* 				Small fixes, cleanup etc.
*/

/****************************************************************************************************
	DEFINES & INCLUDES
*****************************************************************************************************/
#pragma semicolon 1
#define PLUGIN_VERSION "2.3"
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

/****************************************************************************************************
	VARIABLES
*****************************************************************************************************/
new Handle:g_hEntPercent = INVALID_HANDLE;
/****************************************************************************************************
	PLUGIN INFO
*****************************************************************************************************/
public Plugin:myinfo = 
{
	name = "Edict Overflow Fix",
	author = "xCoderx",
	version = PLUGIN_VERSION,
	url = "www.bravegaming.net"
}

/****************************************************************************************************
	PLUGIN INIT
*****************************************************************************************************/
public OnPluginStart()
{
	g_hEntPercent 		= CreateConVar("eof_entpercent", "25", "Percentage Limit for an Edict");
	CreateConVar("eof_version", PLUGIN_VERSION, "EOF", FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_DONTRECORD|FCVAR_NOTIFY);
}

/****************************************************************************************************
	PUBLIC FUNCTIONS
*****************************************************************************************************/
public OnEntityCreated(entity, const String:className[])
{
	if(GetEntityCount() >= 1792)
	{
		EntityHandler(className, entity);
		
		if(entity > 2032)
		{
			LogMessage("[EOF] Warning: Less than 16 free edicts available, Killing a %s at slot %d", className, entity);
			AcceptEntityInput(entity, "Kill");
		}
	}
}

public EntityHandler(const String:className[], entity)
{
	new ent = -1;
	while((ent = FindEntityByClassname(ent, className)) != -1)
	{
		if(ent == entity)
			continue;
		
		if(GetPercentage(className) >= GetConVarInt(g_hEntPercent))
		{
			LogMessage("[EOF] An edict went over the %d percent limit, Killing a %s", GetConVarInt(g_hEntPercent), className);
			AcceptEntityInput(ent, "Kill");
		}
	}
}

/****************************************************************************************************
	STOCKS
*****************************************************************************************************/

stock GetPercentage(const String:className[])
{
	new iTotalCount = GetEntityCount(), iClassCount = GetCount(className);
	return RoundToNearest((float(iClassCount) / float(iTotalCount)) * 100.0);
}

stock GetCount(const String:className[])
{
	new count = 0, entity = -1;
	
	while ((entity = FindEntityByClassname(entity, className)) != INVALID_ENT_REFERENCE)
		count++;
	
	return count;
}