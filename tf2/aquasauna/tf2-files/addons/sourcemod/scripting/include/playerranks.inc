#if defined _playerranks_
  #endinput
#endif
#define _playerranks_

public SharedPlugin:__pl_playerranks = 
{
	name = "Player Ranks",
	file = "playerranks.smx",
	#if defined REQUIRE_PLUGIN
		required = 1,
	#else
		required = 0,
	#endif
	
};

public __pl_playerranks_SetNTVOptional()
{	
	MarkNativeAsOptional("PR_Reward");
	MarkNativeAsOptional("PR_Revoke");
	MarkNativeAsOptional("PR_IsClientLoaded");
	
	MarkNativeAsOptional("PR_GetAssists");
	MarkNativeAsOptional("PR_GetBackstabs");
	MarkNativeAsOptional("PR_GetBossDamage");
	MarkNativeAsOptional("PR_GetDeaths");
	MarkNativeAsOptional("PR_GetFeigns");
	MarkNativeAsOptional("PR_GetHeadshots");
	MarkNativeAsOptional("PR_GetKills");
	MarkNativeAsOptional("PR_GetMerasmusKills");
	MarkNativeAsOptional("PR_GetMerasmusLevel");
	MarkNativeAsOptional("PR_GetMonoculusKills");
	MarkNativeAsOptional("PR_GetMonoculusLevel");
	MarkNativeAsOptional("PR_GetRoundEligibility");
	MarkNativeAsOptional("PR_GetRoundPoints");
	MarkNativeAsOptional("PR_GetTimePlayed");
	MarkNativeAsOptional("PR_GetTotalPoints");
	
	MarkNativeAsOptional("PR_SetAssists");
	MarkNativeAsOptional("PR_SetBackstabs");
	MarkNativeAsOptional("PR_SetBossDamage");
	MarkNativeAsOptional("PR_SetDeaths");
	MarkNativeAsOptional("PR_SetFeigns");
	MarkNativeAsOptional("PR_SetHeadshots");
	MarkNativeAsOptional("PR_SetKills");
	MarkNativeAsOptional("PR_SetMerasmusKills");
	MarkNativeAsOptional("PR_SetMerasmusLevel");
	MarkNativeAsOptional("PR_SetMonoculusKills");
	MarkNativeAsOptional("PR_SetMonoculusLevel");
	MarkNativeAsOptional("PR_SetRoundEligibility");
	MarkNativeAsOptional("PR_SetRoundPoints");
	MarkNativeAsOptional("PR_SetTotalPoints");
	
	MarkNativeAsOptional("PR_SaveAll");
	MarkNativeAsOptional("PR_ShowRankMenu");
	MarkNativeAsOptional("PR_ShowRankMe");
	MarkNativeAsOptional("PR_ShowTopPlayers");
}

/**
 * Rewards a client a specified amount of points.
 *
 * @param clientId		Client Index
 * @param amount		Amount of points to give to this client.
 * @param forced		If true, this reward ignores the player-minimum restriction.
 */
native PR_Reward(clientId, Float:amount, bool:forced = false);

/**
 * Revokes a specified amount of points from a client.
 *
 * @param clientId		Client Index
 * @param amount		Amount of points to remove from this client.
 * @param forced		If true, the points revoked ignore the player-minimum restriction.
 */
native PR_Revoke(clientId, Float:amount, bool:forced = false);

/**
 * Returns a boolean value indicating if the client's rank is loaded and it is safe to alter their points.
 *
 * @param clientId		Client Index
 * @return True if the client's rank is loaded or false otherwise.
 */
native bool:PR_IsClientLoaded(clientId);


/**
 * Gets the amount of assists the client has in total.
 *
 * @param clientId		Client Index
 * @return Assist count. (Int)
 */
native PR_GetAssists(clientId);

/**
 * Gets the amount of backstabs that the client has in total.
 *
 * @param clientId		Client Index
 * @return Backstab count. (Int)
 */
native PR_GetBackstabs(clientId);

/**
 * Gets the amount of damage that the client has done to the currently spawned boss.
 *
 * @param clientId		Client Index
 * @return Damage amount. (Int)
 */
native PR_GetBossDamage(clientId);

/**
 * Gets the amount of deaths total by client.
 *
 * @param clientId		Client Index
 * @return Death count. (Int)
 */
native PR_GetDeaths(clientId);

/**
 * Gets the amount of backstabs total by client.
 *
 * @param clientId		Client Index
 * @return Backstab count. (Int)
 */
native PR_GetFeigns(clientId);

/**
 * Gets the amount of headshots total by client.
 *
 * @param clientId		Client Index
 * @return Headshot count. (Int)
 */
native PR_GetHeadshots(clientId);

/**
 * Gets the amount of kills total by client.
 *
 * @param clientId		Client Index
 * @return Kill count. (Int)
 */
native PR_GetKills(clientId);

/**
 * Gets the amount of Merasmus kills that the client currently has.
 *
 * @param clientId		Client Index
 * @return Merasmus kill count. (Int)
 */
native PR_GetMerasmusKills(clientId);

/**
 * Gets the highest level of Merasmus that the client has killed.
 *
 * @param clientId		Client Index
 * @return Merasmus's level. (Int)
 */
native PR_GetMerasmusLevel(clientId);

/**
 * Gets the amount of Monoculus kills that the client currently has.
 *
 * @param clientId		Client Index
 * @return Monoculus kill count. (Int)
 */
native PR_GetMonoculusKills(clientId);

/**
 * Gets the highest level of Monoculus that the client has killed.
 *
 * @param clientId		Client Index
 * @return Monoculus's level. (Int)
 */
native PR_GetMonoculusLevel(clientId);

/**
 * Gets whether the client will be allowed to receive the round bonus.
 * Clients are flagged as eligible when the round starts and the plugin detects connected clients.
 *
 * @param clientId		Client Index
 * @return True if eligible, false if not. (Boolean)
 */
native bool:PR_GetRoundEligibility(clientId);

/**
 * Gets the amount of points the client has for this round.
 *
 * @param clientId		Client Index
 * @return Point amount. (Float)
 */
native Float:PR_GetRoundPoints(clientId);

/**
 * Gets the client's time played in total.
 *
 * @param clientId		Client Index
 * @return Total time (in seconds). (Float)
 */
native Float:PR_GetTimePlayed(clientId);

/**
 * Gets the amount of points the client has in total.
 *
 * @param clientId		Client Index
 * @return Point amount. (Float)
 */
native Float:PR_GetTotalPoints(clientId);

/**
 * Sets the amount of assists the client has.
 *
 * @param clientId		Client Index
 */
native PR_SetAssists(clientId, assists);

/**
 * Sets the amount of backstabs the client has.
 *
 * @param clientId		Client Index
 */
native PR_SetBackstabs(clientId, backstabs);

/**
 * Sets the amount of damage the client has done to the boss.
 *
 * @param clientId		Client Index
 */
native PR_SetBossDamage(clientId, damage);

/**
 * Sets the amount of deaths the client has.
 *
 * @param clientId		Client Index
 */
native PR_SetDeaths(clientId, deaths);

/**
 * Sets the amount of feigned deaths the client has.
 *
 * @param clientId		Client Index
 */
native PR_SetFeigns(clientId, feigns);

/**
 * Sets the amount of headshots the client has.
 *
 * @param clientId		Client Index
 */
native PR_SetHeadshots(clientId, headshots);

/**
 * Sets the amount of kills the client has.
 *
 * @param clientId		Client Index
 */
native PR_SetKills(clientId, kills);

/**
 * Sets the amount of Merasmus kills the client has.
 *
 * @param clientId		Client Index
 */
native PR_SetMerasmusKills(clientId, kills);

/**
 * Sets the highest level Merasmus the client has killed.
 *
 * @param clientId		Client Index
 */
native PR_SetMerasmusLevel(clientId, level);

/**
 * Sets the amount of Monoculus assists the player has.
 *
 * @param clientId		Client Index
 */
native PR_SetMonoculusKills(clientId, kills);

/**
 * Sets the highest level of Monoculus the client has killed.
 *
 * @param clientId		Client Index
 */
native PR_SetMonoculusLevel(clientId, level);

/**
 * Sets if the player is eligible for the round bonus.
 *
 * @param clientId		Client Index
 * @param eligible		Boolean, if true the player is eligible to receive the bonus.
 */
native PR_SetRoundEligibility(clientId, bool:eligible);

/**
 * Sets the amount of points the client has earned for this round.
 *
 * @param clientId		Client Index
 */
native PR_SetRoundPoints(clientId, Float:points);

/**
 * Sets the amount of points the client has earned total.
 *
 * @param clientId		Client Index
 */
native PR_SetTotalPoints(clientId, Float:points);

/**
 * Saves the score of all connected clients.
 */
native PR_SaveAll();

/**
 * Shows the rank menu.
 */
native PR_ShowRankMenu(clientId);

/**
 * Shows your stats
 */
native PR_ShowRankMe(clientId);

/**
 * Shows the top players menu.
 */
native PR_ShowTopPlayers(clientId);