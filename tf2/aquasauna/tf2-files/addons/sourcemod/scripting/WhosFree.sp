#include <sourcemod>
#define AUTOLOAD_EXTENSIONS
#define REQUIRE_EXTENSIONS
#include <steamtools>

#define PLUGIN_VERSION "1.0.0"

public Plugin:myinfo = {
	name        = "[Any] Who's Free?",
	author      = "DarthNinja",
	description = "List Free2Play players",
	version     = PLUGIN_VERSION,
	url         = "DarthNinja.com"
};

public OnPluginStart()
{
	CreateConVar("sm_whosfree_version", PLUGIN_VERSION, "Who's free?", FCVAR_DONTRECORD|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	RegConsoleCmd("sm_whosfree", WhosFree, "List free2play players");
	RegConsoleCmd("whosfree", WhosFree, "List free2play players");
	LoadTranslations("common.phrases");
}


public Action:WhosFree(client, args)
{
	if (args != 1)
	{
		for (new i=1; i<=MaxClients; i++)
		{
			if (!IsClientAuthorized(i) || IsFakeClient(i))
			{
				continue;
			}
			if (Steam_CheckClientSubscription(i, 0) && !Steam_CheckClientDLC(i, 459))
			{
				ReplyToCommand(client, "%N - Free2Play", i);
			}
			else
			{
				ReplyToCommand(client, "%N - Premium", i);
			}
		}
	}
	else
	{
		decl String:buffer[32];
		GetCmdArg(1, buffer, sizeof(buffer))
		new i = FindTarget(client, buffer, true, false);
		if (!IsClientAuthorized(i) || i == -1)
		{
			ReplyToCommand(client, "Player cannot be found!");
			return Plugin_Handled;
		}
		if (Steam_CheckClientSubscription(i, 0) && !Steam_CheckClientDLC(i, 459))
		{
			ReplyToCommand(client, "%N - Free2Play", i);
		}
		else
		{
			ReplyToCommand(client, "%N - Premium", i); 
		}
	}
	return Plugin_Handled;
}