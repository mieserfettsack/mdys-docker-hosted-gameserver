"MaxClassPlayers"
{
    //Default config if the map tree is not found
    "default"
    {
            //When there is 1 to 8 players in the server
            "1-14"
            {
                 //Team2 = red AND team3 = blue
                 //set to 0 if you want this to only work on one team
                 "red"        "1"
                 "blue"        "1"
             
                //Put -1 for no limit, 0 for disalowed, and any other number to set the limit
                "scout"        "2"
                "sniper"    "2"
                "soldier"    "3"
                "demoman"    "3"
                "medic"        "-1"
                "heavyweapons"        "2"
                "pyro"        "2"
                "spy"        "2"
                "engineer"    "2"
            }
            //When there is 1 to 8 players in the server
            "15-24"
            {
                 //Team2 = red AND team3 = blue
                 //set to 0 if you want this to only work on one team
                 "red"        "1"
                 "blue"        "1"
             
                //Put -1 for no limit, 0 for disalowed, and any other number to set the limit
                "scout"        "3"
                "sniper"    "2"
                "soldier"    "4"
                "demoman"    "4"
                "medic"        "-1"
                "heavyweapons"        "3"
                "pyro"        "3"
                "spy"        "2"
                "engineer"    "3"
            }

    }
            //Anything that is not set, will be -1 (no limit)
    
    Put the map name as the name for specif-map config
    "koth_lakeside_event"
    {
        //When there is 1 to 5 players in the server
        "1-5"
        {
             //If the team is not set, it will be 1
            //Put -1 for no limit, 0 for disalowed, and any other number to set the limit
            "scout"        "-1"
            "sniper"    "-1"
            "soldier"    "-1"
            "demoman"    "-1"
            "medic"        "-1"
            "heavyweapons"        "-1"
            "pyro"        "-1"
            "spy"        "-1"
            "engineer"    "-1"
        }
        
        //When there is 6 players in the server
        "6"
        {
             //If the team is not set, it will be 1
            //Put -1 for no limit, 0 for disalowed, and any other number to set the limit
            "scout"        "-1"
            "sniper"    "-1"
            "soldier"    "-1"
            "demoman"    "-1"
            "medic"        "-1"
            "heavyweapons"        "-1"
            "pyro"        "-1"
            "spy"        "-1"
            "engineer"    "-1"
        }
        //Anything that is not set, will be -1 (no limit)
    }
}
