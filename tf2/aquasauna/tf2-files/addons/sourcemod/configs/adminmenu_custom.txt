// Custom admin menu commands.
// For more information:
//
// http://wiki.alliedmods.net/Custom_Admin_Menu_%28SourceMod%29
//
// Note: This file must be in Valve KeyValues format (no multiline comments)
//

"Commands"
{
"ServerCommands"
	{
		"Restart Server"
		{
			"cmd"		"_restart"
			"execute"	"server"
		}
		"Toggle Tournament Mode"
	        {
	        	"cmd"		"mp_tournament #1"
	        	"1"
	        	{
	        		"type"	"onoff"
	        		"title"	"Toggle Tournament Mode:"
	        	}
	        }
	}
"PlayerCommands"
	{
      "Kick new reason"
        {
            "cmd"        "sm_kick #1 @2"
            "1"
            {
                "title"        "Kick"
                "type"        "player"
            }
            "2"
            {
                "title"        "Kick"
                "type"        "list"
                "1"            "Abusive"
                "2"            "Racism"
                "3"            "General cheating/exploits"
                "4"            "Mic spamming"
                "5"            "Admin disrepect"
                "6"            "Unacceptable Spray"
                "7"            "Breaking Server Rules"
                "8"            "Other"
                "9"            "Idling, blocking Server slot"
            }
        }
		"Scramble Teams Vote"
		  {
			"cmd"		"sm_scramblevote now"
		  }
      "Scramble Teams"
        {
         "cmd"    "sm_scramble"
        }
      "Whos free"
        {
         "cmd"    "whosfree"
        }
	}
"Map Commands"
        {
		"O: Change Map"
		{
			"cmd"		"changelevel #1"
			"execute"	"server"
			"1"
			{
				"type"	"mapcycle"
				"path"	"officialmaps.txt"
				"title"	"Choose Map:"
			}
		}
		"O: Set Nextmap"
		{
			"cmd"		"sm_nextmap #1"
			"execute"	"server"
			"1"
			{
				"type"	"mapcycle"
				"path"	"officialmaps.txt"
				"title"	"Choose nextmap:"
			}
		}

	        "Set Nextmap"
	        {
	                "cmd"           "sm_nextmap #1"
	                "execute"       "server"
	                "1"
	                {
	                        "type"  "mapcycle"
        	                "path"  "maplist_full.txt"
	                        "title" "Choose nextmap:"
	                }
	        }
		 "Change Map"
                {
                        "cmd"           "changelevel #1"
                        "execute"       "server"
                        "1"
                        {
                                "type"  "mapcycle"
                                "path"  "maplist_full.txt"
                                "title" "Choose Map:"
                        }
                }
		

        }
}

