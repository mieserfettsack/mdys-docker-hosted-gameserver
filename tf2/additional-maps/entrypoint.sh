#!/bin/bash

/usr/local/bin/aws s3 sync s3://mdys-docker-hosted-gameserver/tf2/maps /root/maps
find /root/maps -type f -iname "*.bsp" -exec bzip2 -1 -k {} \;
/usr/local/bin/aws s3 sync /root/maps s3://mdys-docker-hosted-gameserver/tf2/maps --acl public-read
find /root/maps -type f -iname "*.bz2" -exec rm -f {} \;
rsync -avcz -e ssh /root/maps/ PRODUCTION_USER@PRODUCTION_SERVER_IP:/var/lib/docker/maps
ssh PRODUCTION_USER@PRODUCTION_SERVER_IP "chown -R 1337:1337 /var/lib/docker/maps"
ssh PRODUCTION_USER@PRODUCTION_SERVER_IP "chmod -R 775 /var/lib/docker/maps"
